// const request = require('supertest');
// const app = require('./src/index'); // Importa tu aplicación Express
// const {
//   getParkings,
//   getParking,
//   createParking,
//   updateParking,
//   deleteParking,
// } = require('./src/controllers/parkingsController');

// // Mock de datos para las pruebas
// const mockParking = { id: '0', name: 'Test Parking', spots: 100 };

// describe('Parking Controller Tests', () => {

//   test('GET /parkings should return all parkings', async () => {
//     const response = await request(app).get('/parkings');
//     expect(response.status).toBe(200);
//     expect(response.body.parkings).toHaveLength(1);
//   });

//   return

//   let parkings;

//   beforeEach(() => {
//     // Resetea los datos antes de cada prueba
//     parkings.length = 0;
//   });

//   test('GET /parkings should return all parkings', async () => {
//     parkings.push(mockParking);
//     const response = await request(app).get('/parkings');
//     expect(response.status).toBe(200);
//     expect(response.body.parkings).toHaveLength(1);
//   });

//   test('GET /parkings/:id should return a specific parking', async () => {
//     parkings.push(mockParking);
//     const response = await request(app).get(`/parkings/${mockParking.id}`);
//     expect(response.status).toBe(200);
//     expect(response.body.parking).toEqual(mockParking);
//   });

//   test('GET /parkings/:id should return 404 if parking is not found', async () => {
//     const response = await request(app).get('/parkings/invalid_id');
//     expect(response.status).toBe(404);
//   });

//   test('POST /parkings should create a new parking', async () => {
//     const response = await request(app).post('/parkings').send(mockParking);
//     expect(response.status).toBe(201);
//     expect(response.body.parking).toEqual(expect.objectContaining(mockParking));
//   });

//   test('PUT /parkings/:id should update an existing parking', async () => {
//     parkings.push(mockParking);
//     const updatedParking = { ...mockParking, name: 'Updated Parking' };
//     const response = await request(app).put(`/parkings/${mockParking.id}`).send(updatedParking);
//     expect(response.status).toBe(200);
//     expect(response.body.parking).toEqual(expect.objectContaining(updatedParking));
//   });

//   test('PUT /parkings/:id should return 404 if parking is not found', async () => {
//     const response = await request(app).put('/parkings/invalid_id').send(mockParking);
//     expect(response.status).toBe(404);
//   });

//   test('DELETE /parkings/:id should delete an existing parking', async () => {
//     parkings.push(mockParking);
//     const response = await request(app).delete(`/parkings/${mockParking.id}`);
//     expect(response.status).toBe(200);
//     expect(response.body.parking).toEqual(expect.objectContaining(mockParking));
//     expect(parkings).toHaveLength(0);
//   });

//   test('DELETE /parkings/:id should return 404 if parking is not found', async () => {
//     const response = await request(app).delete('/parkings/invalid_id');
//     expect(response.status).toBe(404);
//   });
// });
