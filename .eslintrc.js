module.exports = {
  env: {
    es2021: true,
    browser: true,
    node: true,
    es6: true,
  },
  "extends": "eslint:recommended",
  "parserOptions": {
      "ecmaVersion": "latest",
      "sourceType": "module"
  },
  globals: {
    window: true,
    module: true
  },
  plugins: [ '@eslint-config-prettier', 'prettier', 'import' ]
}