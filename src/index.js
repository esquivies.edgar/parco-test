require('dotenv').config();

const express = require("express");
const bodyParser = require('body-parser');
const errorHandler = require('./middlewares/errorHandler');
const sequelize = require('./utils/database');
const app = express();

// BodyParser config
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded({ extended: true }) );

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH');
  next();
});

app.get('/', (req, res, next) => {
  res.send('Hello Parco client')
});

app.use('/users', require('./routes/usersRoutes'));
app.use('/parkings', require('./routes/parkingsRoutes'));
app.use('/checkin', require('./routes/checkInRoutes'));


/* ======================= 
      ERRORS
======================== */
app.use( errorHandler );



/* ======================= 
      DB CONNECTION  
======================== */
// sequelize.sync({force: true})
sequelize.sync()
  .then(() => {
    console.log('Database synced successfully');
    app.listen(3000);

    (()=>{
      // testing();
    })()
  })
  .catch(err => {
    console.error('Unable to sync database:', err);
  });





/* ======================= 
      TESTING
======================== */

async function testing() {
  const User = require('./models/userModel');
  const Parking = require('./models/parkingModel');
  try {
    const usersToCreate = 1010;
  
    for (let i = 1000; i <= usersToCreate; i++) {
      const name = `user${i}`;
      const email = `user${i}@example.com`;
      const password = `user${i}@example.com`;
  
      User.create({
        name: name,
        email: email,
        password: password
      })
        .then(result => {})
        .catch(err => {});
    }
  } catch (error) {}
  try {
    const parkingsToCreate = 1040;
  
    for (let i = 1000; i <= parkingsToCreate; i++) {
      const name = `parking${i}`;
      const spots = i;
      const contact = `332211223344`;
      const parkingType = (i % 2 === 0) ? 'public' : ' private' ;
  
      Parking.create({
        name: name,
        spots: spots,
        contact: contact,
        parkingType: i == 1000 ? 'courtesy' : parkingType
      })
        .then(result => {})
        .catch(err => {});
    }
  } catch (error) {}
}



module.exports = app;