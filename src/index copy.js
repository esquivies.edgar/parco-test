require('dotenv').config();

const express = require("express");
const bodyParser = require('body-parser');
const errorHandler = require('./middlewares/errorHandler');
const sequelize = require('./utils/database');
const app = express();

// BodyParser config
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded({ extended: true }) );

app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH');
  next();
});

app.get('/', (req, res, next) => {
  res.send('Hello Parco client')
});

app.use('/users', require('./routes/usersRoutes'));
app.use('/parkings', require('./routes/parkingsRoutes'));
app.use('/checkin', require('./routes/checkInRoutes'));


/* ======================= 
      ERRORS
======================== */
app.use( errorHandler );



/* ======================= 
      DB CONNECTION  
======================== */
// sequelize.sync({force: true})
sequelize.sync()
  .then(() => {
    console.log('Database synced successfully');
    app.listen(3000);
  })
  .catch(err => {
    console.error('Unable to sync database:', err);
  })
  .finally(() => {
    process.exit(0);
  });

module.exports = app;