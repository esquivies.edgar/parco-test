const Sequelize = require('sequelize');
const Parking = require('../models/parkingModel');

exports.checkIn = async (req, res, next) => {
  try {
    const { parkingId, userType } = req.params;

    const parking = await Parking.findByPk(parkingId);

    let status = 'success';
    let errorCode = null;
    let message = 'CheckIn success.';

    if( parking.parkingType ?? '' !== '' ) {
      return res.status(400).json({ status: 'error', errorCode: 'INVALID_PARKING', message: 'Parking no exist.' });
    }

    // ParkingType validation
    if ( parking.parkingType === 'private' ) {
      if (userType !== 'corporate' ) {
        status = 'error';
        errorCode = 'INVALID_USER_TYPE';
        message = 'Only corporate users can enter private parking lots.';
      } else {
        const today = new Date();
        const dayOfWeek = today.getDay();
        if (!(dayOfWeek >= 1 && dayOfWeek <= 5)) {
          status = 'error';
          errorCode = 'INVALID_DAY';
          message = 'Private parking spaces can only be used on business days (Monday to Friday).';
        }
      }
    } else if (parking.parkingType === 'courtesy') {
      if (userType !== 'visitor') {
        status = 'error';
        errorCode = 'INVALID_USER_TYPE';
        message = 'Only visitor users can enter the courtesy parking lots.';
      } else {
        const today = new Date();
        const dayOfWeek = today.getDay();
        if (!(dayOfWeek === 0 || dayOfWeek === 6)) {
          status = 'error';
          errorCode = 'INVALID_DAY';
          message = 'Visitor parking is only available on weekends.';
        }
      }
    } else if (parking.parkingType !== 'public') {
      status = 'error';
      errorCode = 'INVALID_PARKING_TYPE';
      message = 'Invalid parking type.';
    } 
    
    // UserType validation
    if (!['corporate', 'provider', 'visitor'].includes(userType)) {
      status = 'error';
      errorCode = 'INVALID_PARKING_TYPE';
      message = 'Invalid parking type.';
    }

    return res.status(status === 'success' ? 200 : 400).json({ status, errorCode, message });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ status: 'error', errorCode: 'INTERNAL_ERROR', message: 'Internal Server Error.' });
  }
}