const Parking = require('../models/parkingModel');

// Middlewares
const { generateToken, refreshToken } = require('../middlewares/jwt');

exports.getParkings = (req, res, next) => {
  const { skip = 0, limit = 10, order = 'name', orderDirection = 'ASC' } = req.query;
  console.log(skip,limit,order,orderDirection);
  const skipValue = parseInt(skip, 10);
  const limitValue = parseInt(limit, 10);

  if ( orderDirection !== 'ASC' && orderDirection !== 'DESC' ) {
    return res.status(400).json({ message: 'Invalid pagination parameters' });
  }

  if ( isNaN(skipValue) || isNaN(limitValue) || skipValue < 0 || limitValue <= 0 ) {
    return res.status(400).json({ message: 'Invalid pagination parameters' });
  }

  const validOrderProperties = ['name', 'spots', 'contact', 'parkingType'];
  if ( !validOrderProperties.includes(order) ) {
    return res.status(400).json({ message: 'Invalid order parameter.' });
  }

  const options = {
    offset: skipValue,
    limit: limitValue,
    order: [[order, orderDirection]],
  };

  Parking.findAndCountAll(options)
    .then(result => {
      res.status(200).json({
        totalItems: result.count,
        data: result.rows
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ message: 'Error paginated parking lots.' });
    });
};

exports.getParking = (req, res, next) => {
  const parkingId = req.params.parkingId;
  Parking.findByPk(parkingId)
    .then(parking => {
      if (!parking) {
        return res.status(404).json({ message: 'Parking not found' });
      }

      res.status(200).json({ parking: parking });
    })
    .catch(err => console.log(err));
};

exports.createParking = (req, res, next) => {
  const name = req.body.name;
  const spots = req.body.spots;
  const contact = req.body.contact;
  const parkingType = req.body.parkingType;

  if(
    ((name ?? '') == '') &&
    ((spots ?? '') == '') &&
    ((contact ?? '') == '') &&
    ((parkingType ?? '') == '')
  ) {
    return res.status(400).json({ message: 'fields are missing' });
  }

  // Validate the number of spots for Test
  if (spots < 50) {
    return res.status(400).json({ message: 'Parking is too small(min 50 spots).' });
  } else if (spots > 1500) {
    return res.status(400).json({ message: 'Parking is too large(max 1500 spots).' });
  }

  Parking.create({
    name: name,
    spots: spots,
    contact: contact,
    parkingType: parkingType
  })
    .then(result => {
      res.status(201).json({
        message: 'Parking created successfully!',
        parking: result
      });
    })
    .catch(err => {
      if (err.name === 'SequelizeUniqueConstraintError') {
        return res.status(400).json({ message: 'Parking name is already in use.' });
      }
      console.log(err);
    });
};

exports.updateParking = (req, res, next) => {
  const parkingId = req.params.parkingId;
  const updatedSpots = req.body.spots;
  const updatedContact = req.body.contact;

  Parking.findByPk(parkingId)
    .then(parking => {
      if (!parking) {
        return res.status(404).json({ message: 'Parking not found!' });
      }

      // Validate the number of spots for Test
      if (updatedSpots < 50) {
        return res.status(400).json({ message: 'Parking is too small(min50 spots).' });
      } else if (updatedSpots > 1500) {
        return res.status(400).json({ message: 'Parking is too large(max1500 spots).' });
      }

      // Only filds modifications for Test
      parking.spots = updatedSpots;
      parking.contact = updatedContact;

      return parking.save();
    })
    .then(result => {
      res.status(200).json({ message: 'Parking updated!', parking: result });
    })
    .catch(err => {
      if (err.name === 'SequelizeUniqueConstraintError') {
        return res.status(400).json({ message: 'Parking name is already in use.' });
      }
      console.log(err);
    });
};

exports.deleteParking = (req, res, next) => {
  const parkingId = req.params.parkingId;
  Parking.findByPk(parkingId)
    .then(parking => {
      if (!parking) {
        return res.status(404).json({ message: 'Parking not found!' });
      }

      return Parking.destroy({
        where: {
          id: parkingId
        }
      });
    })
    .then(result => {
      res.status(200).json({ message: 'Parking deleted!' });
    })
    .catch(err => console.log(err));
};
