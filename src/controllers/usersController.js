const User = require('../models/userModel')
const bcrypt = require('bcryptjs');
const saltRounds = 10;

// Middlewares
const { generateToken, refreshToken } = require('../middlewares/jwt')

exports.login = async (req, res, next) => {
  const { email, password } = req.body;
  const attemptsLimit = 8;
  const expirationTime = 10; // Minutes

  try {
    console.log(email.toLowerCase());
    const user = await User.findOne({
      where: {
        email: email.toLowerCase()
      },
    });
    
    if (!user) {
      res.status(204).send();
    } else {
      /* The user has exceeded the limit number of failed login attempts 
      and has been temporarily blocked */
      if (user.blocked) {
        if (new Date() > new Date(user.blockedExpiration)) {
          user.blocked = false;
          await user.save();
        } else {
          return res.status(429).send();
        }
      }
      console.log(password, user.password);
      if (!bcrypt.compareSync(password, user.password)) {
        res.status(204).send();
      } else {
        console.log(user.id, 'user');
        const token = generateToken(user.id, 'user');
        console.log(token);
        res.status(200).json({
          _id: user.id,
          email: user.email,
          token,
        });
      }
    }
  } catch (error) {
    console.error('Error during login:', error);
    res.status(500).send('Internal Server Error');
  }
};

exports.getUsers = async ( req, res, next ) => {
  User.findAll()
    .then(users => {
      res.status(200).json({ users: users})
    })
    .catch(err => console.log(err))
}

exports.getUser = async ( req, res, next ) => {
  const userId = req.params.userId;

  if( isNaN(req.params.userId) ) {
    return res.status(404).json({ message: 'User not found' });
  }

  User.findByPk(userId)
    .then(user => {
      if( !user ) {
        return res.status(404).json({ message: 'User not found' });
      }

      res.status(200).json({ user: user})
    })
    .catch(err => console.log(err))
}

exports.createUser = async ( req, res, next ) => {
  const name = req.body.name
  const email = req.body.email
  const password = req.body.password

  if(
    ((name ?? '') == '') &&
    ((password ?? '') == '') &&
    ((email ?? '') == '')
  ) {
    return res.status(400).json({ message: 'fields are missing' })
  }

  User.create({
    name: name,
    email: email,
    password: password
  })
    .then(result => {
      res.status(201).json({
        message: 'User created!',
        user: result
      })
    })
    .catch(err => {res.status(400).json({
      message: 'User duplicated!',
      err: err
    })})
}

exports.updateUser = async (req, res, next) => {
  const userId = req.params.userId
  const updatedName = req.body.name
  const updatedEmail = req.body.email
  User.findByPk(userId)
    .then(user => {
      if (!user) {
        return res.status(404).json({ message: 'User not found!' })
      }
      user.name = updatedName
      user.email = updatedEmail
      return user.save()
    })
    .then(result => {
      res.status(200).json({message: 'User updated!', user: result})
    })
    .catch(err => console.log(err))
}

exports.deleteUser = async (req, res, next) => {
  const userId = req.params.userId
  User.findByPk(userId)
    .then(user => {
      if (!user) {
        return res.status(404).json({ message: 'User not found!' })
      }
      return User.destroy({
        where: {
          id: userId
        }
      })
    })
    .then(result => {
      res.status(200).json({ message: 'User deleted!' })
    })
    .catch(err => console.log(err))
}