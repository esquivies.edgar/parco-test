const CheckIn = require('../services/checkIn');
const router = require('express').Router();

router.get('/:parkingId/:userType', CheckIn.checkIn);

module.exports = router;