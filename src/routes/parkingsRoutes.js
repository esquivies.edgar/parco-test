const controller = require('../controllers/parkingsController');
const router = require('express').Router();
const { verifyToken } = require("../middlewares/jwt");

router.get('/', verifyToken('user'), controller.getParkings);
router.get('/:parkingId', verifyToken('user'), controller.getParking);
router.post('/', verifyToken('user'), controller.createParking);
router.put('/:parkingId', verifyToken('user'), controller.updateParking);
router.delete('/:parkingId', verifyToken('user'), controller.deleteParking);

module.exports = router;