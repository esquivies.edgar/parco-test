const controller = require('../controllers/usersController');
const router = require('express').Router();
const { verifyToken } = require("../middlewares/jwt");

router.post( '/login', controller.login );
router.get('/', controller.getUsers);
router.get('/:userId', controller.getUser);
router.post('/', controller.createUser);
router.put('/:userId', verifyToken('user'), controller.updateUser);
router.delete('/:userId', controller.deleteUser);

module.exports = router;