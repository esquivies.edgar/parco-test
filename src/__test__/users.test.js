const request = require('supertest');
const app = require('../index'); // Importa tu aplicación Express
// const {
//   getUsers,
//   getUser,
//   createUser,
//   updateUser,
//   deleteUser,
// } = require('../controllers/usersController');

// beforeAll(async () => {
//   await sequelize.sync(); // { force: true }
// });

// afterAll(async () => {
//   await sequelize.close();
// });

// Mock de datos para las pruebas
const mockUser = { id: 1, name: 'userTest', email: 'userTest', password: 'userTest' };
const mockUserCreate = { name: 'userTest', email: 'userTest@gmail.com', password: 'userTest' };

describe('User Controller Tests', () => {

  test('GET /users should return all users', async () => {
    const response = await request(app).get('/users');
    expect(response.status).toBe(200);
    expect(response.body.users.length).toBeGreaterThanOrEqual(1);

  });

  test('GET /users/:id should return a specific user', async () => {
    const response = await request(app).get(`/users/${mockUser.id}`);
    expect(response.status).toBe(200);
    expect(response.body.user.id).toEqual(mockUser.id);

  });

  test('GET /users/:id should return 404 if user is not found', async () => {
    const response = await request(app).get('/users/invalid_id');
    expect(response.status).toBe(404);
  });

  test('POST /users should create a new user', async () => {
    const response = await request(app).post('/users').send(mockUserCreate);
    expect(response.status).toBe(201);
    expect(response.body.user).toEqual(expect.objectContaining({name: mockUserCreate.name, email: mockUserCreate.email}));
  });

//   test('PUT /users/:id should update an existing user', async () => {
//     users.push(mockUser);
//     const updatedUser = { ...mockUser, name: 'Updated User' };
//     const response = await request(app).put(`/users/${mockUser.id}`).send(updatedUser);
//     expect(response.status).toBe(200);
//     expect(response.body.user).toEqual(expect.objectContaining(updatedUser));
//   });

//   test('PUT /users/:id should return 404 if user is not found', async () => {
//     const response = await request(app).put('/users/invalid_id').send(mockUser);
//     expect(response.status).toBe(404);
//   });

  // test('DELETE /users/:id should delete an existing user', async () => {
  //   users.push(mockUser);
  //   const response = await request(app).delete(`/users/${mockUser.id}`);
  //   expect(response.status).toBe(200);
  //   expect(response.body.user).toEqual(expect.objectContaining(mockUser));
  //   expect(users).toHaveLength(0);
  // });

//   test('DELETE /users/:id should return 404 if user is not found', async () => {
//     const response = await request(app).delete('/users/invalid_id');
//     expect(response.status).toBe(404);
//   });

  // afterAll(async () => {
  //   await sequelize.close();
  // });

});
