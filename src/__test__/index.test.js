const request = require('supertest');
const app = require('../index');
const sequelize = require('../utils/database');

// beforeAll(async () => {
//   await sequelize.sync(); //{ force: true }
// });

// afterAll(async () => {
//   await sequelize.close();
// });

describe('Test for rute "/"', () => {
  test('It should return a message "Hello Parco client"', async () => {
    const response = await request(app).get('/');
    expect(response.status).toBe(200);
    expect(response.text).toBe('Hello Parco client');
  });

  // afterAll(async () => {
  //   await sequelize.close();
  // });
});
