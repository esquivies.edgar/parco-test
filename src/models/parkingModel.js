const { DataTypes } = require('sequelize')
const db = require('../utils/database.js');

const Parking = db.define('Parking', {
  name: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  spots: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  contact: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  parkingType: {
    type: DataTypes.ENUM('public', 'private', 'courtesy'),
    allowNull: false,
  }
});

module.exports = Parking;
