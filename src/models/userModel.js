const Sequelize = require('sequelize');
const db = require('../utils/database.js');
const bcrypt = require('bcryptjs');
const saltRounds = 10;

const User = db.define('user', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  name: Sequelize.STRING,
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  },
  password: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  lastName: {
    type: Sequelize.STRING,
  },
  avatar: {
    type: Sequelize.STRING,
  },
  verificationCode: {
    type: Sequelize.STRING,
  },
  verificationExpiration: {
    type: Sequelize.DATE,
  },
  verificationAttempts: {
    type: Sequelize.INTEGER,
  },
  loginAttempts: {
    type: Sequelize.INTEGER,
    defaultValue: 0,
  },
  blocked: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  blockedExpiration: {
    type: Sequelize.DATE,
  },
  deleted: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
});

// Middleware
User.beforeCreate(async (user, options) => {
  await hashPassword(user, options);
});

User.beforeUpdate(async (user, options) => {
  await hashPassword(user, options);
});

async function hashPassword(user, options) {
  if (user.changed('password')) {
    const hashedPassword = await bcrypt.hash(user.password, saltRounds);
    user.password = hashedPassword;
  }
}


module.exports = User;