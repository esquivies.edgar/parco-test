
// const logger = require('./logs');
const logger = require('../utils/logger');


const errorHandler = ( err, req, res, next ) => {
  const mssg = `${err.message}. Stack Trace: ${err.stack}\n`;
  
  logger.error( mssg );
  res.status(500).send();
}

module.exports = errorHandler;
