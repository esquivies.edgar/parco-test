const jwt = require('jsonwebtoken');
const expirationTime = 5; // Minutes

const generateToken = (userId, type = 'user') => {
  const payload = {
    user: userId,
    exp: Math.floor(Date.now() / 1000) + (expirationTime * 60),
  };
  const key = process.env.JWT_SECRET;

  return jwt.sign(payload, key);
};

const refreshToken = (req, type = 'user') => {
  let token = req.headers.authorization || null;

  if (token) {
    token = token.split(' ')[1];
  }

  try {
    const key = process.env.JWT_SECRET;
    const decoded = jwt.verify(token, key, { ignoreExpiration: false });

    token = generateToken(decoded.user, type);
  } catch (error) {
    token = false;
  }

  return token;
}

const verifyToken = (type = 'user') => {
  return (req, res, next) => {
    let token = req.headers.authorization;

    if (!token) {
      return res.status(401).send({ message: "Access denied" });
    } else {
      token = token.split(' ')[1];
    }

    try {
      const key = process.env.JWT_SECRET;
      const decoded = jwt.verify(token, key);

      // Check if the token has expired
      if (decoded.exp < Math.floor(Date.now() / 1000)) {
        return res.status(401).send({ message: "Token has expired" });
      }

      req.userId = decoded.user;
      next();
    } catch (error) {
      console.log(error);
      return res.status(401).send({ message: "Invalid token" });
    }
  }
};

module.exports = {
  generateToken,
  refreshToken,
  verifyToken,
};
